@extends('welcome')
@section('title', 'About')
@section('page_description',
    'Boxity is a leading provider of business software solutions. We help businesses of all
    sizes improve their efficiency, productivity, and profitability.')

@section('icon',
    'https://res.cloudinary.com/boxity-id/image/upload/w_1000/q_auto:best/f_auto/v1678791753/asset_boxity/logo/icon-web_qusdsv.png')
@section('content')
    <section class="page-title page-title-parallax parallax scroll-detect dark include-header" style="padding: 250px 0;">
        <img src="https://res.cloudinary.com/boxity-id/image/upload/w_1000/q_auto:best/f_auto/v1703748942/parallax_tuucku.jpg"
            class="parallax-bg">
        <div class="container">
            <div class="page-title-row">

                <div class="page-title-content">
                    <h1>About Us</h1>
                    <span>Everything you need to know about our Company</span>
                </div>

                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Pages</a></li>
                        <li class="breadcrumb-item active" aria-current="page">About Us</li>
                    </ol>
                </nav>

            </div>
        </div>
    </section><!-- .page-title end -->
    <section id="content">
        <div class="content-wrap pb-0">
            <div class="container">
                <div class="row col-mb-50 mb-0">
                    <div class="col-lg-12">

                        <div class="heading-block fancy-title border-bottom-0 title-bottom-border">
                            <h4>About Boxity</h4>
                        </div>

                        <p>
                        <h4 class="m-1">Empowering Businesses with Affordable ERP & SaaS Solutions</h4>
                        Founded in 2020, Boxity ID revolutionizes the Indonesian market by providing accessible and
                        high-quality ERP and SaaS solutions. We cater to businesses of all sizes, empowering them to
                        streamline operations, boost efficiency, and drive growth.
                        <br><br>

                        Officially established on January 17, 2022, by the Ministry of Law and Security as PT Boxity
                        Central Indonesia, and headquartered in Jakarta, Boxity ID operates with a dedicated team of
                        experts passionate about helping businesses thrive.
                        <br><br>
                        Our comprehensive suite of cloud-based modules empowers you to manage and optimize every aspect
                        of your operations:
                        </p>
                        <div class="row col-mb-50 mb-0">
                            <div class="col-lg-4 p-5">
                                <img src="https://res.cloudinary.com/boxity-id/image/upload/w_1000/q_auto:best/f_auto/v1703749906/octansidn-white_w2ggif.png"
                                    alt="OctansIDN Finance" title="Boxity's Product" class="img-fluid">
                            </div>
                            <div class="col-lg-4 p-5">
                                <img src="https://res.cloudinary.com/boxity-id/image/upload/w_1000/q_auto:best/f_auto/v1703749595/crater_ceywne.png"
                                    alt="Crater Invoicing" title="Boxity's Product" class="img-fluid">
                            </div>
                            <div class="col-lg-4 p-5">
                                <img src="https://res.cloudinary.com/boxity-id/image/upload/w_1000/q_auto:best/f_auto/v1703749824/logo_secondaryERP_d9v4yb.png"
                                    alt="BoxityID ERP" title="Boxity's Product" class="img-fluid">
                            </div>
                        </div>
                        <p>Beyond our robust features, Boxity ID provides additional value through
                        </p>
                        <ul>
                            <li><b>Seamless Implementation</b><br>Our experienced team ensures a smooth and efficient
                                transition
                                to your
                                new ERP/SaaS system.</li><br>
                            <li><b>Unwavering Support</b><br> We offer comprehensive support through dedicated teams and
                                readily
                                available
                                resources, ensuring your success.</li><br>
                            <li><b>Continuous Learning</b><br> Leverage our comprehensive training programs to equip your
                                team
                                with
                                the
                                knowledge and skills to harness the full potential of our solutions.</li><br>
                            <li><b>Boxity ID</b> More than just ERP/SaaS, we are your trusted partner in transforming your
                                business,
                                unlocking endless possibilities for growth.</li>
                        </ul>

                    </div>

                    <div class="col-lg-4">

                        <div class="heading-block fancy-title border-bottom-0 title-bottom-border">
                            <h4>Why choose <span>Us</span>.</h4>
                        </div>

                        <p>Skip the complexity, not the features. BoxityID delivers a powerful, easy-to-use ERP solution at
                            an accessible price. Our intuitive interface and advanced functionalities let you manage your
                            business efficiently, saving you time and effort.
                            <br><br>
                            More than just ERP, we empower:
                            <br>
                        </p>
                        <ul>
                            <li><b>MSMEs</b><br>Tailored programs like Octans IDN simplify financial management for
                                individuals and
                                small
                                businesses.</li><br>
                            <li><b>Growth</b><br>Crater streamlines invoice management and integrates directly with customer
                                emails
                                for
                                effortless payment handling.</li><br>
                            <li><b>Learning</b><br>Masterclasses and seminars equip you with the knowledge to make the most
                                of
                                Boxity
                                and
                                similar applications.</li>
                        </ul>
                        <h4><span>Don't let ERP hold you back.</span> Choose BoxityID and experience the difference.</h4>

                    </div>

                    <div class="col-lg-4">

                        <div class="heading-block fancy-title border-bottom-0 title-bottom-border">
                            <h4>Our <span>Vision</span>.</h4>
                        </div>

                        <p>Streamline your operations, boost efficiency, and unlock growth with our integrated, affordable,
                            and user-friendly ERP & SaaS solutions for any business.</p>

                    </div>

                    <div class="col-lg-4">

                        <div class="heading-block fancy-title border-bottom-0 title-bottom-border">
                            <h4>What we <span>Do</span>.</h4>
                        </div>

                        <h4 class="m-0">Beyond Building Powerful ERP Solutions</h4>
                        We go further than creating isolated tools. We research real-world needs, collaborate with
                        industry partners, and craft impactful programs for individuals and businesses of all sizes.
                        <br><br>
                        <h4 class="m-0">
                            Empowering Every Step
                        </h4>
                        Recognizing complex ERP workflows don't always fit every reality, we bridge the gap. Through
                        engaging seminars and masterclasses, we demystify digitalization and equip you with the
                        knowledge to thrive in today's tech-driven world.
                        <br><br>
                        <h4 class="m-0">Our impact</h4>
                        <ul>
                            <li><b>Tailored programs</b><br>Octans IDN simplifies financial management for individuals and
                                small
                                businesses.</li><br>
                            <li><b>Frictionless payments</b><br>Crater streamlines invoice management and integrates with
                                customer
                                emails
                                for effortless payment handling.</li><br>
                            <li><b>Knowledge is power</b><br>Masterclasses and seminars unlock the full potential of your
                                digital
                                tools.</li>
                        </ul>

                    </div>

                </div>
            </div>
            <div class="section"
                style="margin-bottom:0; background: url('https://res.cloudinary.com/boxity-id/image/upload/w_1000/q_auto:best/f_auto/b_rgb:000000,o_60/v1709978345/abbe-sublett-nxZDMUQhN4o-unsplash_vaxdu4.jpg') no-repeat center center / cover;">
                <div class="row text-center justify-content-center align-items-center min-vh-75 m-0">
                    <div class="col-md-8">
                        <h2 class="display-4 fw-semibold text-white">Unlock the potential of your business with BoxityERP's
                            innovative solutions, seamlessly blending innovation, simplicity, and efficiency. Dive deeper
                            into our story by downloading our company profile and get to know us better.</h2>
                        <div><a href="/profile" class="button text-black bg-color rounded-pill m-0 h-op-09 px-4"><i
                                    class="bi bi-cloud-download"></i> Download
                                profile</a></div>
                    </div>
                </div>
            </div>
            <div class="section m-0">
                <div class="container">

                    <div class="row col-mb-50">

                        <div class="col-sm-6 col-lg-3 text-center" data-animate="bounceIn">
                            <i class="i-plain i-xlarge mx-auto bi-people"></i>
                            <div class="counter counter-lined"><span data-from="0" data-to="112" data-refresh-interval="5"
                                    data-speed="2000"></span></div>
                            <h5>Customers</h5>
                        </div>

                        <div class="col-sm-6 col-lg-3 text-center" data-animate="bounceIn" data-delay="200">
                            <i class="i-plain i-xlarge mx-auto mb-0 bi-bezier"></i>
                            <div class="counter counter-lined"><span data-from="0" data-to="1543" data-refresh-interval="5"
                                    data-speed="2500"></span>K</div>
                            <h5>Transactions</h5>
                        </div>

                        <div class="col-sm-6 col-lg-3 text-center" data-animate="bounceIn" data-delay="400">
                            <i class="i-plain i-xlarge mx-auto mb-0 bi-layers"></i>
                            <div class="counter counter-lined"><span data-from="0" data-to="4" data-refresh-interval="2"
                                    data-speed="3500"></span></div>
                            <h5>Services</h5>
                        </div>

                        <div class="col-sm-6 col-lg-3 text-center" data-animate="bounceIn" data-delay="600">
                            <i class="i-plain i-xlarge mx-auto mb-0 bi-percent"></i>
                            <div class="counter counter-lined"><span data-from="0" data-to="9" data-refresh-interval="3"
                                    data-speed="2700"></span>/10</div>
                            <h5>Level of satisfaction</h5>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </section>
@endsection
