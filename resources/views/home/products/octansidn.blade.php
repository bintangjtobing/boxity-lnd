@extends('welcome')
@section('title', 'OctansIDN')
@section('page_description',
    'Octansidn Finance is a cloud-based financial management solution that helps businesses of
    all sizes to streamline their financial processes, improve visibility, and make better decisions.')

@section('icon',
    'https://res.cloudinary.com/boxity-id/image/upload/w_1000/q_auto:best/f_auto/v1678791753/asset_boxity/logo/icon-web_qusdsv.png')
@section('content')
    <section class="page-title bg-transparent">
        <div class="container">
            <div class="page-title-row">

                <div class="page-title-content">
                    <img src="https://res.cloudinary.com/boxity-id/image/upload/w_1000/q_auto:best/f_auto/v1703749906/octansidn-white_w2ggif.png"
                        alt="Modular App by Boxity"
                        srcset="https://res.cloudinary.com/boxity-id/image/upload/w_1000/q_auto:best/f_auto/v1703749906/octansidn-white_w2ggif.png"
                        class="img-fluid" style="max-width: 350px;">
                </div>

                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Products</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Octans IDN</li>
                    </ol>
                </nav>

            </div>
        </div>
    </section><!-- .page-title end -->
    <section id="content">
        <div class="content-wrap pt-0">
            <div class="section mt-0">
                <div class="container">

                    <div class="row col-mb-50">
                        <div class="col-md-8">
                            <img data-animate="fadeIn" class="aligncenter"
                                src="https://res.cloudinary.com/boxity-id/image/upload/w_1000/q_auto:best/f_auto/v1703757786/octans-mac-ip_wrf44r.png"
                                alt="Octans IDN View" style="max-width: 700px;">
                        </div>

                        <div class="col-md-4">
                            <div class="feature-box fbox-plain my-5">
                                <div class="fbox-icon">
                                    <a href="#"><i class="i-alt">1.</i></a>
                                </div>
                                <div class="fbox-content">
                                    <h3>Simple and easy-to-understand interface</h3>
                                    <p>Users can easily understand how to use this application.</p>
                                </div>
                            </div>
                            <div class="feature-box fbox-plain my-5">
                                <div class="fbox-icon">
                                    <a href="#"><i class="i-alt">2.</i></a>
                                </div>
                                <div class="fbox-content">
                                    <h3>Dynamic, and complete feature</h3>
                                    <p>Octans has features for recording income and expenses, generating financial reports,
                                        and customizing reports.</p>
                                </div>
                            </div>
                            <div class="feature-box fbox-plain my-5">
                                <div class="fbox-icon">
                                    <a href="#"><i class="i-alt">3.</i></a>
                                </div>
                                <div class="fbox-content">
                                    <h3>Affordable and suit for SME's</h3>
                                    <p>Octans offers a free trial for 30 days. After the trial, users can subscribe for a
                                        monthly plan starting at $4.00</p>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <div class="container">

                <div class="row align-items-center col-mb-50">
                    <div class="col-md-7">
                        <canvas id="chart-doughnut"></canvas>
                    </div>

                    <div class="col-md-5 text-center text-md-start">
                        <div class="heading-block border-bottom-0">
                            <h4>Unleash financial superpowers:</h4>
                            <span>Octans IDN is more than just software. It's your financial co-pilot, navigating you to
                                explosive growth.
                            </span>
                        </div>

                        <ul>
                            <li><b>Command your finances</b><br> Track and analyze every penny with laser precision. Product
                                sales,
                                orders, payments – it's all at your fingertips.</li><br>
                            <li><b>Dive deep, discover gold</b><br> Go beyond the numbers. Boxity ERP's insightful reports
                                reveal
                                hidden
                                trends, empowering data-driven decisions.</li><br>
                            <li><b>Harness the marketing maelstrom</b><br> Target those millennial eco-warriors with
                                pinpoint
                                accuracy.
                                Boxity ERP fuels winning marketing campaigns that skyrocket sales.</li><br>
                            <li><b>From struggling to soaring</b><br> Watch that eco-friendly underdog transform into a
                                roaring
                                success
                                story, data-fueled by Boxity ERP.</li>
                        </ul>
                    </div>
                </div>

                <div class="line"></div>

                <div class="row align-items-center col-mb-50">
                    <div class="col-md-7 order-md-last">
                        <canvas id="chart-radar"></canvas>
                    </div>

                    <div class="col-md-5 text-center text-md-start">
                        <div class="heading-block border-bottom-0">
                            <h4>25% More Efficient? Upgrade Your Finance Now!</h4>
                            <span>Master your finances, master your business. The world is becoming more digital, and manual
                                financial systems are becoming outdated. Increase your financial workflow efficiency by 25%
                                with intelligent financial applications.</span>
                        </div>

                        <p>Don't let outdated financial systems hold you back. Switch to intelligent applications, achieve
                            25% more financial greatness, and create a manufacturing champion!
                            <br><br>
                            Upgrade to intelligent financial applications now!
                        </p>
                    </div>
                </div>

            </div>
            <a href="https://uat.octansidn.com/register" target="_blank"
                class="button button-full text-center text-end mt-6 footer-stick">
                <div class="container">
                    Wanna try a demo? <strong>Start here</strong> <i class="fa-solid fa-caret-right" style="top:4px;"></i>
                </div>
            </a>
        </div>
    </section><!-- #content end -->
@endsection
@section('script')
    <script>
        var randomScalingFactor = function() {
            return Math.round(Math.random() * 100);
        };

        var config = {
            type: 'line',
            data: {
                labels: [
                    "Machine Uptime",
                    "Production Output",
                    "Quality Rate",
                    "Maintenance Costs",
                    "Inventory Turnover",
                    "Energy Efficiency"
                ],
                datasets: [{
                    label: "Not Using OctansIdn",
                    data: [
                        85,
                        92,
                        89,
                        78,
                        81,
                        89
                    ],
                    borderColor: 'rgba(237, 84, 149, .8)',
                    backgroundColor: 'rgba(237, 84, 149, .2)'
                }, {
                    label: "After Using OctansIdn",
                    data: [
                        92,
                        96,
                        98,
                        72,
                        85,
                        92
                    ],
                    borderColor: 'rgba(67, 43, 167, .8)',
                    backgroundColor: 'rgba(67, 43, 167, .2)'
                }]
            },
            options: {
                responsive: true,
                legend: {
                    display: true,
                },
                title: {
                    display: false,
                    text: 'Line Chart Kinerja Bulanan Penggunaan Aplikasi Keuangan Octansidn'
                },
                animation: {
                    animateScale: true,
                    animateRotate: true
                }
            }
        };

        // Radar Chart
        var color = Chart.helpers.color;
        var configRadar = {
            type: 'radar',
            data: {
                labels: [
                    "Machine Uptime",
                    "Production Output",
                    "Quality Rate",
                    "Maintenance Costs",
                    "Inventory Turnover",
                    "Energy Efficiency"
                ],
                datasets: [{
                    label: "Not Using OctansIdn",
                    data: [
                        85,
                        92,
                        89,
                        78,
                        81,
                        89
                    ],
                    borderColor: 'rgba(237, 84, 149, .8)',
                    backgroundColor: 'rgba(237, 84, 149, .2)'
                }, {
                    label: "After Using OctansIdn",
                    data: [
                        96,
                        96,
                        98,
                        70,
                        85,
                        92
                    ],
                    borderColor: 'rgba(67, 43, 167, .8)',
                    backgroundColor: 'rgba(67, 43, 167, .2)'
                }]
            },
            options: {
                responsive: true,
                legend: {
                    display: true,
                },
                title: {
                    display: false,
                    text: 'Radar Chart Kinerja Bulanan Penggunaan Aplikasi Keuangan Octansidn'
                },
                animation: {
                    animateScale: true,
                    animateRotate: true
                }
            }
        };



        window.onload = function() {
            var ctx = document.getElementById("chart-doughnut").getContext("2d");
            window.myDoughnut = new Chart(ctx, config);
            window.myRadar = new Chart(document.getElementById("chart-radar"), configRadar);
        };
    </script>
@endsection
