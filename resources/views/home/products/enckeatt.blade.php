@extends('welcome')
@section('title', 'Encke Attendance')
@section('page_description',
    'Encke Attendance by Boxity is a cloud-based attendance management solution that helps
    businesses improve productivity, efficiency, and compliance. Our solution is easy to use, scalable, and affordable.')

@section('icon',
    'https://res.cloudinary.com/boxity-id/image/upload/w_1000/q_auto:best/f_auto/v1678791753/asset_boxity/logo/icon-web_qusdsv.png')
@section('content')
    <section class="page-title bg-transparent">
        <div class="container">
            <div class="page-title-row">

                <div class="page-title-content">
                    <img src="https://res.cloudinary.com/boxity-id/image/upload/w_1000/q_auto:best/f_auto/v1703764390/enckeatt_tc2b48.png"
                        alt="Modular App by Boxity"
                        srcset="https://res.cloudinary.com/boxity-id/image/upload/w_1000/q_auto:best/f_auto/v1703764390/enckeatt_tc2b48.png"
                        class="img-fluid" style="max-width: 350px;">
                </div>

                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Products</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Encke ATT</li>
                    </ol>
                </nav>

            </div>
        </div>
    </section><!-- .page-title end -->
    <section id="content">
        <div class="content-wrap pt-0">
            <div class="section mt-0">
                <div class="container">

                    <div class="row col-mb-50">
                        <div class="col-md-8">
                            <img data-animate="fadeIn" class="aligncenter"
                                src="https://res.cloudinary.com/boxity-id/image/upload/w_1000/q_auto:best/f_auto/v1703765046/encke_copy_qvtfmk.png"
                                alt="Encke Attendance App - View" style="max-width: 700px;">
                        </div>

                        <div class="col-md-4">
                            <div class="feature-box fbox-plain my-5">
                                <div class="fbox-icon">
                                    <a href="#"><i class="i-alt">1.</i></a>
                                </div>
                                <div class="fbox-content">
                                    <h3>Accuracy</h3>
                                    <p>Encke Attendance uses a variety of sensors to accurately track employee attendance.
                                        This includes QR Code. This ensures that employee attendance records are accurate
                                        and reliable.</p>
                                </div>
                            </div>
                            <div class="feature-box fbox-plain my-5">
                                <div class="fbox-icon">
                                    <a href="#"><i class="i-alt">2.</i></a>
                                </div>
                                <div class="fbox-content">
                                    <h3>Flexibility</h3>
                                    <p>Encke Attendance is a flexible system that can be customized to meet the needs of any
                                        business. It can be used to track employee attendance, timesheets, and more. This
                                        makes it a versatile tool that can be used for a variety of purposes.</p>
                                </div>
                            </div>
                            <div class="feature-box fbox-plain my-5">
                                <div class="fbox-icon">
                                    <a href="#"><i class="i-alt">3.</i></a>
                                </div>
                                <div class="fbox-content">
                                    <h3>Affordability</h3>
                                    <p>Encke Attendance is a cost-effective solution for businesses of all sizes. It offers
                                        a variety of pricing options to fit any budget. This makes it a viable option for
                                        businesses of all sizes.</p>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <div class="container">

                <div class="row align-items-center col-mb-50">
                    <div class="col-md-7">
                        <canvas id="chart-doughnut"></canvas>
                    </div>

                    <div class="col-md-5 text-center text-md-start">
                        <div class="heading-block border-bottom-0">
                            <h4>Unleash a Financial Revolution with EnckeAttendance from Boxity!</h4>
                            <span>Stop merely tracking attendance, start unlocking explosive business growth with
                                EnckeAttendance, the Boxity attendance solution that's more than just a clock-in clock-out
                                system.
                            </span>
                        </div>

                        <ul>
                            <li><b>Boost Productivity</b><br> EnckeAttendance eliminates time theft and streamlines
                                workflows, igniting employee efficiency like never before.</li><br>
                            <li><b>Slash Costs</b><br> Shed the burden of manual attendance tracking. Say goodbye to wasted
                                hours and inaccurate payrolls, maximizing your financial firepower.</li><br>
                            <li><b>Unleash Insights</b><br> Dive deeper than ever before with real-time attendance
                                analytics. Identify trends, optimize schedules, and empower data-driven decisions that
                                propel your business forward.</li><br>
                        </ul>
                    </div>
                </div>

            </div>
            <a href="mailto:info@boxity.id" target="_blank"
                class="button button-full text-center text-end mt-6 footer-stick">
                <div class="container">
                    Wanna try a demo? <strong>Contact us</strong> <i class="fa-solid fa-caret-right" style="top:4px;"></i>
                </div>
            </a>
        </div>
    </section><!-- #content end -->
@endsection
@section('script')
    <script>
        var randomScalingFactor = function() {
            return Math.round(Math.random() * 100);
        };

        var config = {
            type: 'line',
            data: {
                labels: [
                    "Machine Uptime",
                    "Production Output",
                    "Quality Rate",
                    "Maintenance Costs",
                    "Inventory Turnover",
                    "Energy Efficiency"
                ],
                datasets: [{
                    label: "Before EnckeAttendance",
                    data: [
                        85,
                        92,
                        89,
                        78,
                        81,
                        89
                    ],
                    borderColor: 'rgba(237, 84, 149, .8)',
                    backgroundColor: 'rgba(237, 84, 149, .2)'
                }, {
                    label: "After EnckeAttendance",
                    data: [
                        92,
                        96,
                        93,
                        72,
                        85,
                        92
                    ],
                    borderColor: 'rgba(67, 43, 167, .8)',
                    backgroundColor: 'rgba(67, 43, 167, .2)'
                }]
            },
            options: {
                responsive: true,
                legend: {
                    display: true,
                },
                title: {
                    display: true,
                    text: 'Line Chart Kinerja Harian Penggunaan Aplikasi Attendance Boxity'
                },
                animation: {
                    animateScale: true,
                    animateRotate: true
                }
            }
        };





        window.onload = function() {
            var ctx = document.getElementById("chart-doughnut").getContext("2d");
            window.myDoughnut = new Chart(ctx, config);
        };
    </script>
@endsection
