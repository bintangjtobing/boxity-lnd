@extends('welcome')
@section('title', 'Craters app')
@section('page_description',
    'Craters Embedded Invoicing & Bill Pay for Platforms: Simplify and automate your invoicing
    and bill pay processes with Craters by Boxity.')

@section('icon',
    'https://res.cloudinary.com/boxity-id/image/upload/w_1000/q_auto:best/f_auto/v1678791753/asset_boxity/logo/icon-web_qusdsv.png')
@section('content')
    <section class="page-title bg-transparent">
        <div class="container">
            <div class="page-title-row">

                <div class="page-title-content">
                    <img src="https://res.cloudinary.com/boxity-id/image/upload/w_1000/q_auto:best/f_auto/v1703749595/crater_ceywne.png"
                        alt="Crater by Boxity"
                        srcset="https://res.cloudinary.com/boxity-id/image/upload/w_1000/q_auto:best/f_auto/v1703749595/crater_ceywne.png"
                        class="img-fluid" style="max-width: 350px;">
                </div>

                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Products</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Crater</li>
                    </ol>
                </nav>

            </div>
        </div>
    </section><!-- .page-title end -->
    <section id="content">
        <div class="content-wrap pt-0">
            <div class="section mt-0">
                <div class="container">

                    <div class="row col-mb-50">
                        <div class="col-md-8">
                            <img data-animate="fadeIn" class="aligncenter"
                                src="https://res.cloudinary.com/boxity-id/image/upload/w_1000/q_auto:best/f_auto/v1703765995/crater-mac-ip_gwtodn.png"
                                alt="Crater App View" style="max-width: 700px;">
                        </div>

                        <div class="col-md-4">
                            <div class="feature-box fbox-plain my-5">
                                <div class="fbox-icon">
                                    <a href="#"><i class="i-alt">1.</i></a>
                                </div>
                                <div class="fbox-content">
                                    <h3>Invoicing & Bill Pay</h3>
                                    <p>Intuitive invoicing workflows to pay & get paid.</p>
                                </div>
                            </div>
                            <div class="feature-box fbox-plain my-5">
                                <div class="fbox-icon">
                                    <a href="#"><i class="i-alt">2.</i></a>
                                </div>
                                <div class="fbox-content">
                                    <h3>Lending</h3>
                                    <p>AP & AR tools to solve your customer's cashflow needs.</p>
                                </div>
                            </div>
                            <div class="feature-box fbox-plain my-5">
                                <div class="fbox-icon">
                                    <a href="#"><i class="i-alt">3.</i></a>
                                </div>
                                <div class="fbox-content">
                                    <h3>Payments</h3>
                                    <p>Simplified onboarding, ACH & card acceptance.</p>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <div class="container">

                <div class="row align-items-center col-mb-50">
                    <div class="col-md-5">
                        <img src="https://res.cloudinary.com/boxity-id/image/upload/w_1000/q_auto:best/f_auto/v1703766350/invoicing-workflows_rmtl6p.png"
                            style="max-width: auto" alt="Modular App by Boxity" class="img-fluid">
                    </div>

                    <div class="col-md-7 text-center text-md-start">
                        <div class="heading-block border-bottom-0">
                            <h4>Invoicing Workflows </h4>
                            <span>Robust invoicing infrastructure to send and receive invoices. Manage all Accounts
                                Receivable & Accounts Payable in one place with ability to accept deposits on estimates.
                            </span>
                        </div>
                    </div>
                </div>


                <div class="row align-items-center col-mb-50">
                    <div class="col-md-5 order-md-last">
                        <img src="https://res.cloudinary.com/boxity-id/image/upload/w_1000/q_auto:best/f_auto/v1703766350/PaymentsIcon_zx6p1t.svg"
                            style="max-width: auto" alt="Modular App by Boxity" class="img-fluid">
                    </div>

                    <div class="col-md-7 text-center text-md-start">
                        <div class="heading-block border-bottom-0">
                            <h4>Payments</h4>
                            <span>Empower your users to pay & get paid with fully integrated & easy to launch payments via
                                card or ACH. Pre-built integration with Plaid allows users to easily connect bank accounts
                                to make payments. </span>
                        </div>
                    </div>
                </div>
                <div class="row align-items-center col-mb-50">
                    <div class="col-md-5">
                        <img src="https://res.cloudinary.com/boxity-id/image/upload/w_1000/q_auto:best/f_auto/v1703766350/lending_vah21c.png"
                            style="max-width: auto" alt="Modular App by Boxity" class="img-fluid">
                    </div>

                    <div class="col-md-7 text-center text-md-start">
                        <div class="heading-block border-bottom-0">
                            <h4>Lending</h4>
                            <span>Fully integrated working capital tools to solve your users’ cashflow problems. Provide
                                advances on Accounts Receivable or enable customers to extend payment terms on their AP with
                                ‘Buy Now, Pay Later’.
                            </span>
                        </div>
                    </div>
                </div>

            </div>
            <a href="https://crater.boxity.id" target="_blank"
                class="button button-full text-center text-end mt-6 footer-stick">
                <div class="container">
                    Wanna try a demo? <strong>Start here</strong> <i class="fa-solid fa-caret-right" style="top:4px;"></i>
                </div>
            </a>
        </div>
    </section><!-- #content end -->
@endsection
@section('script')
    <script>
        var randomScalingFactor = function() {
            return Math.round(Math.random() * 100);
        };

        var config = {
            type: 'line',
            data: {
                labels: [
                    "Machine Uptime",
                    "Production Output",
                    "Quality Rate",
                    "Maintenance Costs",
                    "Inventory Turnover",
                    "Energy Efficiency"
                ],
                datasets: [{
                    label: "Not Using OctansIdn",
                    data: [
                        85,
                        92,
                        89,
                        78,
                        81,
                        89
                    ],
                    borderColor: 'rgba(237, 84, 149, .8)',
                    backgroundColor: 'rgba(237, 84, 149, .2)'
                }, {
                    label: "After Using OctansIdn",
                    data: [
                        92,
                        96,
                        98,
                        72,
                        85,
                        92
                    ],
                    borderColor: 'rgba(67, 43, 167, .8)',
                    backgroundColor: 'rgba(67, 43, 167, .2)'
                }]
            },
            options: {
                responsive: true,
                legend: {
                    display: true,
                },
                title: {
                    display: false,
                    text: 'Line Chart Kinerja Bulanan Penggunaan Aplikasi Keuangan Octansidn'
                },
                animation: {
                    animateScale: true,
                    animateRotate: true
                }
            }
        };

        // Radar Chart
        var color = Chart.helpers.color;
        var configRadar = {
            type: 'radar',
            data: {
                labels: [
                    "Machine Uptime",
                    "Production Output",
                    "Quality Rate",
                    "Maintenance Costs",
                    "Inventory Turnover",
                    "Energy Efficiency"
                ],
                datasets: [{
                    label: "Not Using OctansIdn",
                    data: [
                        85,
                        92,
                        89,
                        78,
                        81,
                        89
                    ],
                    borderColor: 'rgba(237, 84, 149, .8)',
                    backgroundColor: 'rgba(237, 84, 149, .2)'
                }, {
                    label: "After Using OctansIdn",
                    data: [
                        96,
                        96,
                        98,
                        70,
                        85,
                        92
                    ],
                    borderColor: 'rgba(67, 43, 167, .8)',
                    backgroundColor: 'rgba(67, 43, 167, .2)'
                }]
            },
            options: {
                responsive: true,
                legend: {
                    display: true,
                },
                title: {
                    display: false,
                    text: 'Radar Chart Kinerja Bulanan Penggunaan Aplikasi Keuangan Octansidn'
                },
                animation: {
                    animateScale: true,
                    animateRotate: true
                }
            }
        };



        window.onload = function() {
            var ctx = document.getElementById("chart-doughnut").getContext("2d");
            window.myDoughnut = new Chart(ctx, config);
            window.myRadar = new Chart(document.getElementById("chart-radar"), configRadar);
        };
    </script>
@endsection
