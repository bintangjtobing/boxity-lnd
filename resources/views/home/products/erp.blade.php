@extends('welcome')
@section('title', 'BoxityERP')
@section('page_description',
    'Boxity ERP is a comprehensive business management solution that helps businesses of all
    sizes to improve their efficiency, productivity, and profitability.')

@section('icon',
    'https://res.cloudinary.com/boxity-id/image/upload/w_1000/q_auto:best/f_auto/v1678791753/asset_boxity/logo/icon-web_qusdsv.png')
@section('content')
    <section class="page-title bg-transparent">
        <div class="container">
            <div class="page-title-row">

                <div class="page-title-content">
                    <img src="https://res.cloudinary.com/boxity-id/image/upload/w_1000/q_auto:best/f_auto/v1703749824/logo_secondaryERP_d9v4yb.png"
                        alt="Modular App by Boxity"
                        srcset="https://res.cloudinary.com/boxity-id/image/upload/w_1000/q_auto:best/f_auto/v1703749824/logo_secondaryERP_d9v4yb.png"
                        class="img-fluid" style="max-width: 350px;">
                </div>

                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Products</a></li>
                        <li class="breadcrumb-item active" aria-current="page">BoxityERP</li>
                    </ol>
                </nav>

            </div>
        </div>
    </section><!-- .page-title end -->
    <section id="content">
        <div class="content-wrap pt-0">
            <div class="section mt-0">
                <div class="container">

                    <div class="heading-block text-center">
                        <h2>Unrivaled Advantages of BoxityERP</h2>
                    </div>

                    <div class="row col-mb-50">
                        <div class="col-12">
                            <img data-animate="fadeIn" class="aligncenter"
                                src="https://res.cloudinary.com/boxity-id/image/upload/w_1000/q_auto:best/f_auto/v1703757629/erpboxity-mac_rk3nal.png"
                                alt="Boxity ERP Dashboard View" style="max-width: 700px;">
                        </div>

                        <div class="col-md-4">
                            <div class="feature-box fbox-plain">
                                <div class="fbox-icon">
                                    <a href="#"><i class="i-alt">1.</i></a>
                                </div>
                                <div class="fbox-content">
                                    <h3>Quickly and efficiently</h3>
                                    <p>Powerful Layout with Responsive functionality that can be adapted.</p>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="feature-box fbox-plain">
                                <div class="fbox-icon">
                                    <a href="#"><i class="i-alt">2.</i></a>
                                </div>
                                <div class="fbox-content">
                                    <h3>Easily understandable</h3>
                                    <p>Globally scale exceptional human capital without
                                        bricks-and-clicks.</p>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="feature-box fbox-plain">
                                <div class="fbox-icon">
                                    <a href="#"><i class="i-alt">3.</i></a>
                                </div>
                                <div class="fbox-content">
                                    <h3>Flexible and customizable</h3>
                                    <p>Appropriately orchestrate leveraged sources whereas enterprise.</p>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <div class="container">

                <div class="row align-items-center col-mb-50">
                    <div class="col-md-5">
                        <canvas id="chart-doughnut"></canvas>
                    </div>

                    <div class="col-md-7 text-center text-md-start">
                        <div class="heading-block border-bottom-0">
                            <h4>Powerful insights to help grow your business.</h4>
                            <span>Reports let you easily track and analyze your product sales, orders, and payments.</span>
                        </div>

                        <p>Uncover powerful insights through reports provided by Boxity ERP. Deep-dive into product sales,
                            orders, and payments, empowering you to make data-driven decisions on marketing strategies,
                            product development, and pricing. Imagine a struggling eco-friendly product sees a surge
                            in
                            sales among millennial customers after targeted campaigns informed by Boxity ERP data. Boxity
                            ERP Transform your business with the power of insights.</p>
                    </div>
                </div>

                <div class="line"></div>

                <div class="row align-items-center col-mb-50">
                    <div class="col-md-5 order-md-last">
                        <canvas id="chart-radar"></canvas>
                    </div>

                    <div class="col-md-7 text-center text-md-start">
                        <div class="heading-block border-bottom-0">
                            <h4>Empower your manufacturing with intelligent application management</h4>
                            <span>Gain data-driven insights by benchmarking your website's performance, enabling you to make
                                informed choices that optimize production, drive profitability, and outperform the
                                competition.</span>
                        </div>

                        <p>Unleash manufacturing excellence with intelligent application management, coupled with
                            data-driven insights gleaned from website benchmarking. Empower your operations with seamless
                            application integration, real-time production visibility, and automated workflows. Gain a
                            competitive edge through website performance insights, informing marketing strategies and
                            optimizing conversion rates. Make informed choices, optimize production, drive profitability,
                            and outperform the competition - all powered by intelligence, data, and your relentless pursuit
                            of manufacturing mastery.</p>
                    </div>
                </div>

            </div>

            <div class="container mt-5">

                <div class="heading-block text-center">
                    <h2>Modules, and features in BoxityERP</h2>
                    <span>Boxity ERP puts the power of professional tools in your hands. Grow your business faster, manage
                        your finances with ease, and gain a competitive edge.</span>
                </div>

                <div class="row col-mb-50">
                    <div class="col-md-4">
                        <div class="feature-box fbox-plain">
                            <div class="fbox-icon">
                                <a href="#"><i class="i-alt bi-house"></i></a>
                            </div>
                            <div class="fbox-content">
                                <h3>Warehouse Management</h3>
                                <p>Warehouse management is the process of managing and controlling the inventory of goods in
                                    a warehouse. This process includes various activities, from receiving goods, storing
                                    goods, and picking up goods, to sending goods.</p><br>
                                <h4 class="m-0">Features</h4>
                                <ul>
                                    <li><b>Goods receipt</b><br> Record and manage the receipt of goods from suppliers.</li>
                                    <br>
                                    <li><b>Goods storage</b><br> Storing goods in a warehouse safely and efficiently.</li>
                                    <br>
                                    <li><b>Picking up goods</b><br> Picking up goods from the warehouse as needed.</li><br>
                                    <li><b>Delivery of goods</b><br> Sending goods to customers according to orders.</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="feature-box fbox-plain">
                            <div class="fbox-icon">
                                <a href="#"><i class="i-alt bi-layers"></i></a>
                            </div>
                            <div class="fbox-content">
                                <h3>Stock Management</h3>
                                <p>Stock management is the process of managing inventory as a whole, starting from planning,
                                    procurement, and storage, to sales.</p><br>
                                <h4 class="m-0">Features</h4>
                                <ul>
                                    <li><b>Inventory planning</b><br> Determining the quantity and type of goods to be
                                        ordered.
                                    </li><br>
                                    <li><b>Procurement of inventory</b><br> Purchasing goods from suppliers. </li><br>
                                    <li><b>Inventory storage</b><br> Storing goods in a warehouse safely and efficiently.
                                    </li>
                                    <br>
                                    <li><b>Inventory sales</b><br> Selling goods to customers. </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="feature-box fbox-plain">
                            <div class="fbox-icon">
                                <a href="#"><i class="i-alt bi-credit-card-2-front"></i></a>
                            </div>
                            <div class="fbox-content">
                                <h3>Payroll Management</h3>
                                <p>Payroll management is the process of managing employee salaries and benefits. This
                                    process includes various activities, from salary calculations, and salary payments, to
                                    payroll reporting.</p><br>
                                <h4 class="m-0">Features</h4>
                                <ul>
                                    <li><b>Salary calculation</b><br> Calculate employee salaries and benefits following
                                        applicable
                                        regulations.</li><br>
                                    <li><b>Salary payments</b><br> Pay employee salaries and benefits according to a
                                        predetermined
                                        schedule.</li><br>
                                    <li><b>Payroll reporting</b><br> Create accurate and complete payroll reports.</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="feature-box fbox-plain">
                            <div class="fbox-icon">
                                <a href="#"><i class="i-alt bi-people"></i></a>
                            </div>
                            <div class="fbox-content">
                                <h3>Employee Management</h3>
                                <p>Employee management is the process of managing employees as a whole, starting from
                                    recruitment, training, and development, to employee dismissal.</p><br>
                                <h4 class="m-0">Features</h4>
                                <ul>
                                    <li><b>Employee recruitment</b><br> Searching for and recruiting employees who suit the
                                        company's
                                        needs. </li><br>
                                    <li><b>Employee training</b><br> Improve employee skills and competencies. </li><br>
                                    <li><b>Employee development</b><br> Helping employees to develop their careers. </li>
                                    <br>
                                    <li><b>Dismissal of employees</b><br> Fairly dismiss employees and by applicable
                                        regulations.
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="feature-box fbox-plain">
                            <div class="fbox-icon">
                                <a href="#"><i class="i-alt bi-layer-backward"></i></a>
                            </div>
                            <div class="fbox-content">
                                <h3>Production Management</h3>
                                <p>Production management is the process of managing the production of goods or services.
                                    This process includes various activities, from production planning, and production
                                    control, to quality control.</p><br>
                                <h4 class="m-0">Features</h4>
                                <ul>
                                    <li><b>Production planning</b><br> Determining the quantity and type of goods or
                                        services to be
                                        produced.</li><br>
                                    <li><b>Production control</b><br> Controlling the production process to ensure the goods
                                        or
                                        services
                                        produced comply with established standards.</li><br>
                                    <li><b>Quality control</b><br> Ensuring that the goods or services produced meet
                                        established
                                        quality
                                        standards.</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="feature-box fbox-plain">
                            <div class="fbox-icon">
                                <a href="#"><i class="i-alt bi-journal-check"></i></a>
                            </div>
                            <div class="fbox-content">
                                <h3>Finance/Accounting Management</h3>
                                <p>Finance/accounting management is the process of managing company finances. This process
                                    includes various activities, from financial recording, and financial reporting, to
                                    financial analysis.</p><br>
                                <h4 class="m-0">Features</h4>
                                <ul>
                                    <li><b>Financial recording</b><br> Accurately recording company financial transactions.
                                    </li><br>
                                    <li><b>Financial reporting</b><br> Create accurate and complete financial reports.</li>
                                    <br>
                                    <li><b>Financial analysis</b><br> Analyzing financial data for decision-making.</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            {{-- <a href="#" class="button button-full text-center text-end mt-6 footer-stick">
                <div class="container">
                    Need help with your Account? <strong>Start here</strong> <i class="fa-solid fa-caret-right"
                        style="top:4px;"></i>
                </div>
            </a> --}}
        </div>
    </section><!-- #content end -->
@endsection
@section('script')
    <script>
        var randomScalingFactor = function() {
            return Math.round(Math.random() * 100);
        };

        var config = {
            type: 'doughnut',
            data: {
                labels: [
                    "Machine Uptime",
                    "Production Output",
                    "Quality Rate",
                    "Maintenance Costs",
                    "Inventory Turnover",
                    "Energy Efficiency",
                ],
                datasets: [{
                    data: [
                        // Gunakan nilai data yang paling relevan untuk powerful insights
                        96, // Quality Rate (contoh)
                        85, // Machine Uptime (contoh)
                        89, // Energy Efficiency (contoh)
                        81, // Inventory Turnover (contoh)
                        88, // Data Accuracy (contoh)
                        78, // Maintenance Costs (contoh)
                    ],
                    backgroundColor: [
                        window.chartColors.red,
                        window.chartColors.orange,
                        window.chartColors.yellow,
                        window.chartColors.green,
                        window.chartColors.blue,
                        window.chartColors.purple
                    ],
                    label: 'Plant A'
                }]
            },
            options: {
                responsive: true,
                legend: {
                    display: true, // Sembunyikan legend untuk tampilan lebih ringkas
                },
                title: {
                    display: true,
                    text: 'Powerful Insights to Help Your Business'
                },
                animation: {
                    animateScale: true,
                    animateRotate: true
                },
                elements: {
                    donut: {
                        labels: {
                            position: 'inside', // Tampilkan label di dalam donut
                        }
                    }
                }
            }
        };




        // Radar Chart
        var color = Chart.helpers.color;
        var configRadar = {
            type: 'radar',
            data: {
                labels: [
                    "Machine Uptime",
                    "Production Output",
                    "Quality Rate",
                    "Maintenance Costs",
                    "Inventory Turnover",
                    "Energy Efficiency",
                    "Data Accuracy"
                ],
                datasets: [{
                    label: "After BoxityERP",
                    backgroundColor: color(window.chartColors.orange).alpha(0.5).rgbString(),
                    borderColor: window.chartColors.orange,
                    pointBackgroundColor: window.chartColors.orange,
                    data: [
                        85, // Machine Uptime
                        92, // Production Output
                        96, // Quality Rate
                        78, // Maintenance Costs
                        81, // Inventory Turnover
                        89, // Energy Efficiency
                        94 // Data Accuracy
                    ]
                }, {
                    label: "Before BoxityERP",
                    backgroundColor: color(window.chartColors.green).alpha(0.5).rgbString(),
                    borderColor: window.chartColors.green,
                    pointBackgroundColor: window.chartColors.green,
                    data: [
                        72, // Machine Uptime
                        85, // Production Output
                        91, // Quality Rate
                        84, // Maintenance Costs
                        75, // Inventory Turnover
                        82, // Energy Efficiency
                        88 // Data Accuracy
                    ]
                }]
            },
            options: {
                legend: {
                    display: true, // Tampilkan legend untuk membedakan pabrik
                    position: 'top'
                },
                title: {
                    display: true,
                    text: 'Manufacturing Performance with Intelligent Applications'
                },
                scale: {
                    ticks: {
                        beginAtZero: true
                    }
                }
            }
        };


        window.onload = function() {
            var ctx = document.getElementById("chart-doughnut").getContext("2d");
            window.myDoughnut = new Chart(ctx, config);
            window.myRadar = new Chart(document.getElementById("chart-radar"), configRadar);
        };
    </script>
@endsection
