@extends('welcome')
@section('title', 'Job Openings')
@section('page_description',
    'Boxity is a technology company focused on developing software solutions for businesses. We are looking for the best
    talent to join our solid and experienced team.

    We offer various positions in various fields, from software development, marketing, sales, to operations. We also offer
    a conducive and enjoyable work environment, as well as opportunities to grow and contribute meaningfully to the company.

    If you are a passionate, creative person who wants to make a positive impact in the world, we invite you to join us.

    ')

@section('icon',
    'https://res.cloudinary.com/boxity-id/image/upload/w_1000/q_auto:best/f_auto/v1678791753/asset_boxity/logo/icon-web_qusdsv.png')
@section('content')
    <section class="page-title page-title-parallax parallax scroll-detect dark">
        <img src="https://res.cloudinary.com/boxity-id/image/upload/w_1000/q_auto:best/f_auto/v1703748942/parallax_tuucku.jpg"
            class="parallax-bg">
        <div class="container">
            <div class="page-title-row">

                <div class="page-title-content">
                    <h1>Job Openings</h1>
                </div>

                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Pages</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Career</li>
                    </ol>
                </nav>

            </div>
        </div>
    </section>
    <section id="content">
        <div class="content-wrap">
            <div class="container">
                <div class="row col-mb-50">
                    <div class="fancy-title title-bottom-border">
                        <p>We appreciate your interest in our company. <br>We currently have no available job openings, but
                            we
                            would be happy to help you find a position that is a good fit for your skills and experience.
                        </p>
                    </div>
                    {{-- <div class="col-md-7">
                        <div class="fancy-title title-bottom-border">
                            <h3>Senior Python Developer</h3>
                        </div>

                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nemo, natus voluptatibus adipisci porro
                            magni dolore eos eius ducimus corporis quos perspiciatis quis iste, vitae autem libero ullam
                            omnis cupiditate quam.</p>

                        <div class="accordion accordion-bg">

                            <div class="accordion-header">
                                <div class="accordion-icon">
                                    <i class="accordion-closed bi-check-circle-fill"></i>
                                    <i class="accordion-open bi-x-circle-fill"></i>
                                </div>
                                <div class="accordion-title">
                                    Requirements
                                </div>
                            </div>
                            <div class="accordion-content">
                                <ul class="iconlist iconlist-color mb-0">
                                    <li><i class="fa-solid fa-check"></i>B.Tech./ B.E / MCA degree in Computer Science,
                                        Engineering or a related stream.</li>
                                    <li><i class="fa-solid fa-check"></i>3+ years of software development experience.</li>
                                    <li><i class="fa-solid fa-check"></i>3+ years of Python / Java development projects
                                        experience.</li>
                                    <li><i class="fa-solid fa-check"></i>Minimum of 4 live project roll outs.</li>
                                    <li><i class="fa-solid fa-check"></i>Experience with third-party libraries and APIs.
                                    </li>
                                    <li><i class="fa-solid fa-check"></i>In depth understanding and experience of either
                                        SDLC or PDLC.</li>
                                    <li><i class="fa-solid fa-check"></i>Good Communication Skills</li>
                                    <li><i class="fa-solid fa-check"></i>Team Player</li>
                                </ul>
                            </div>

                            <div class="accordion-header">
                                <div class="accordion-icon">
                                    <i class="accordion-closed bi-check-circle-fill"></i>
                                    <i class="accordion-open bi-x-circle-fill"></i>
                                </div>
                                <div class="accordion-title">
                                    What we Expect from you?
                                </div>
                            </div>
                            <div class="accordion-content">
                                <ul class="iconlist iconlist-color mb-0">
                                    <li><i class="bi-plus-circle-fill"></i>Design and build applications/ components using
                                        open source technology.</li>
                                    <li><i class="bi-plus-circle-fill"></i>Taking complete ownership of the deliveries
                                        assigned.</li>
                                    <li><i class="bi-plus-circle-fill"></i>Collaborate with cross-functional teams to
                                        define, design, and ship new features.</li>
                                    <li><i class="bi-plus-circle-fill"></i>Work with outside data sources and API's.</li>
                                    <li><i class="bi-plus-circle-fill"></i>Unit-test code for robustness, including edge
                                        cases, usability, and general reliability.</li>
                                    <li><i class="bi-plus-circle-fill"></i>Work on bug fixing and improving application
                                        performance.</li>
                                </ul>
                            </div>

                            <div class="accordion-header">
                                <div class="accordion-icon">
                                    <i class="accordion-closed bi-check-circle-fill"></i>
                                    <i class="accordion-open bi-x-circle-fill"></i>
                                </div>
                                <div class="accordion-title">
                                    What you've got?
                                </div>
                            </div>
                            <div class="accordion-content">You'll be familiar with agile practices and have a highly
                                technical background, comfortable discussing detailed technical aspects of system design and
                                implementation, whilst remaining business driven. With 5+ years of systems analysis,
                                technical analysis or business analysis experience, you'll have an expansive toolkit of
                                communication techniques to enable shared, deep understanding of financial and technical
                                concepts by diverse stakeholders with varying backgrounds and needs. In addition, you will
                                have exposure to financial systems or accounting knowledge.</div>

                        </div>

                        <div class="divider divider-sm"><i class="bi-star-fill"></i></div>
                    </div>
                    <div class="col-md-5">
                        <div id="job-apply" class="heading-block highlight-me">
                            <h2>Apply Now</h2>
                            <span>And we'll get back to you within 48 hours.</span>
                        </div>

                        <div class="form-widget">
                            <div class="form-result"></div>

                            <form action="include/form.php" id="template-jobform" name="template-jobform" class="row mb-0"
                                method="post">

                                <div class="form-process">
                                    <div class="css3-spinner">
                                        <div class="css3-spinner-scaler"></div>
                                    </div>
                                </div>

                                <div class="col-md-6 form-group">
                                    <label for="template-jobform-fname">First Name <small>*</small></label>
                                    <input type="text" id="template-jobform-fname" name="template-jobform-fname"
                                        value="" class="form-control required">
                                </div>

                                <div class="col-md-6 form-group">
                                    <label for="template-jobform-lname">Last Name <small>*</small></label>
                                    <input type="text" id="template-jobform-lname" name="template-jobform-lname"
                                        value="" class="form-control required">
                                </div>

                                <div class="w-100"></div>

                                <div class="col-12 form-group">
                                    <label for="template-jobform-email">Email <small>*</small></label>
                                    <input type="email" id="template-jobform-email" name="template-jobform-email"
                                        value="" class="required email form-control">
                                </div>

                                <div class="col-md-6 form-group">
                                    <label for="template-jobform-age">Age <small>*</small></label>
                                    <input type="text" name="template-jobform-age" id="template-jobform-age"
                                        value="" size="22" tabindex="4" class="form-control required">
                                </div>

                                <div class="col-md-6 form-group">
                                    <label for="template-jobform-city">City <small>*</small></label>
                                    <input type="text" name="template-jobform-city" id="template-jobform-city"
                                        value="" size="22" tabindex="5" class="form-control required">
                                </div>

                                <div class="w-100"></div>

                                <div class="col-12 form-group">
                                    <label for="template-jobform-service">Position <small>*</small></label>
                                    <select name="template-jobform-position" id="template-jobform-position"
                                        tabindex="9" class="form-select required">
                                        <option value="">-- Select Position --</option>
                                        <option value="Senior Python Developer">Senior Python Developer</option>
                                    </select>
                                </div>

                                <div class="col-md-6 form-group">
                                    <label for="template-jobform-salary">Expected Salary</label>
                                    <input type="text" name="template-jobform-salary" id="template-jobform-salary"
                                        value="" size="22" tabindex="6" class="form-control">
                                </div>

                                <div class="col-md-6 form-group">
                                    <label for="template-jobform-time">Start Date</label>
                                    <input type="date" name="template-jobform-start" id="template-jobform-start"
                                        value="" size="22" tabindex="7" class="form-control">
                                </div>

                                <div class="w-100"></div>

                                <div class="col-12 form-group">
                                    <label for="template-jobform-website">Website (if any)</label>
                                    <input type="url" name="template-jobform-website" id="template-jobform-website"
                                        value="" size="22" tabindex="8" class="form-control">
                                </div>

                                <div class="col-12 form-group">
                                    <label for="template-jobform-experience">Cover Letter (optional)</label>
                                    <textarea name="template-jobform-experience" id="template-jobform-experience" rows="3" tabindex="10"
                                        class="form-control"></textarea>
                                </div>

                                <div class="col-12 form-group d-none">
                                    <input type="text" id="template-jobform-botcheck" name="template-jobform-botcheck"
                                        value="" class="form-control">
                                </div>

                                <div class="col-12 form-group">
                                    <button class="button button-3d button-large w-100 m-0" name="template-jobform-apply"
                                        type="submit" value="apply">Send Application</button>
                                </div>

                                <input type="hidden" name="prefix" value="template-jobform-">

                            </form>
                        </div>

                    </div> --}}
                </div>

            </div>
        </div>
    </section><!-- #content end -->
@endsection
