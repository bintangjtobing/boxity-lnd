@extends('welcome')
@section('title', 'Home')
@section('page_description',
    'Providing ERP with cloud-based web and mobile applications and distributing them on a SaaS
    basis for MSME needs.')

@section('icon',
    'https://res.cloudinary.com/boxity-id/image/upload/w_1000/q_auto:best/f_auto/v1678791753/asset_boxity/logo/icon-web_qusdsv.png')
@section('content')
    <section id="content">
        <div class="content-wrap pt-5">

            <div class="container">
                <div class="row col-mb-30 align-items-center text-center text-lg-start">
                    <div class="col-lg-5">
                        <p class="color text-uppercase ls-4 fw-bold small">Welcome to Boxity</p>
                        <h3 class="display-4 fw-bold font-secondary text-white">Elevate Your Business and
                            increasing
                            your process
                            business up to 25% <span class="nocolor bg-color bg-opacity-10">with BoxityID</span>
                        </h3>
                    </div>

                    <div class="col-lg-7">
                        <img src="https://res.cloudinary.com/boxity-id/image/upload/w_1000/q_auto:best/f_auto/v1703267374/resources_new_boxity/s7cjz8jintmgqlqf1ecw.png"
                            alt="Boxity App - Dashboard">
                    </div>
                </div>
            </div>

            <div class="clear mt-6"></div>

            <div class="container">
                <div class="row g-5">
                    <div class="col-sm-6 col-lg-4">
                        <div class="feature-box">
                            <div class="fbox-icon mb-4">
                                <a href="#"><i class="bi-activity color rounded-5"></i></a>
                            </div>
                            <div class="fbox-content">
                                <h3 class="text-transform-none fw-medium text-larger">Quickly and efficiently
                                </h3>
                                <p class="op-08">Powerful Layout with Responsive functionality that can be adapted.
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6 col-lg-4">
                        <div class="feature-box">
                            <div class="fbox-icon mb-4">
                                <a href="#"><i class="bi-lightning-charge color rounded-5"></i></a>
                            </div>
                            <div class="fbox-content">
                                <h3 class="text-transform-none fw-medium text-larger">Easily understandable</h3>
                                <p class="op-08">Globally scale exceptional human capital without
                                    bricks-and-clicks.</p>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6 col-lg-4">
                        <div class="feature-box">
                            <div class="fbox-icon mb-4">
                                <a href="#"><i class="bi-bezier2 color rounded-5"></i></a>
                            </div>
                            <div class="fbox-content">
                                <h3 class="text-transform-none fw-medium text-larger">Flexible and customizable
                                </h3>
                                <p class="op-08">Appropriately orchestrate leveraged sources whereas enterprise.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="section bg-transparent mt-0 pt-0 pt-lg-5">
                <div class="container mw-lg mb-5 text-center text-lg-start">
                    <div class="line clear mb-lg-6"></div>
                    <div class="row justify-content-between">
                        <div class="col-lg-4">
                            <p class="color mb-3">Features that Kill</p>
                            <h2 class="display-5 fw-semibold">Know more how Boxity works integrated</h2>
                        </div>

                        <div class="col-lg-5">
                            <p>Boxity's integrated tech stack, encompassing ERP, Octans finance, and Crater invoicing,
                                empowers data-driven decisions and operational excellence. ERP, Octans finance, and Crater
                                invoicing all play nicely together, giving you the clear view and smarts to boss your
                                business.</p>
                            {{-- <a href="#"
                            class="button text-black bg-color rounded-pill m-0 h-op-09 px-4">Shop Now</a> --}}
                        </div>
                    </div>
                </div>

                <div class="container">
                    <div class="row align-items-stretch g-4">
                        <div class="col-12">
                            <div class="grid-inner bg-white-soft rounded-5"
                                style="background: url('https://res.cloudinary.com/boxity-id/image/upload/w_1000/q_auto:best/f_auto/v1703267358/resources_new_boxity/togufzra9vrvjmggjbzw.jpg') no-repeat center / cover">
                                <div class="p-5">
                                    <h3 class="mb-4 h3 fw-bold text-white">Unrivaled Advantages of BoxityERP</h3>
                                    <ul class="list-inline">
                                        <li class="list-inline-item d-inline-flex align-items-center mb-2 me-4"><i
                                                class="bi-check-circle-fill me-2 color"></i>Warehouse Management:
                                            Super Efficient and Beyond!</li>
                                        <li class="list-inline-item d-inline-flex align-items-center mb-2 me-4"><i
                                                class="bi-check-circle-fill me-2 color"></i>Finance & Accounting
                                            Reports: Beyond Ordinary Insights!</li>
                                        <li class="list-inline-item d-inline-flex align-items-center mb-2 me-4"><i
                                                class="bi-check-circle-fill me-2 color"></i>Payroll Management:
                                            Headache-Free and Prompt!</li>
                                        <li class="list-inline-item d-inline-flex align-items-center mb-2 me-4"><i
                                                class="bi-check-circle-fill me-2 color"></i>Production Management:
                                            Streamline Manufacturing Like Never Before!</li>
                                    </ul>
                                </div>
                                <img src="https://res.cloudinary.com/boxity-id/image/upload/w_1000/q_auto:best/f_auto/v1703267511/project-1_nlvedy.png"
                                    alt="Boxity" class="rounded-5 pt-lg-4 px-lg-6 px-3">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="grid-inner bg-white-soft rounded-5"
                                style="background: url('https://res.cloudinary.com/boxity-id/image/upload/w_1000/q_auto:best/f_auto/v1703267357/resources_new_boxity/krbyujhiuja797zcitfi.jpg') no-repeat center center / cover">
                                <div class="p-5">
                                    <h3 class="mb-4 h3 fw-bold text-white">Boxity - Octans</h3>
                                    <p class="">FinTech Fun for Personal or Small Business Finance!</p>
                                    <ul class="list-inline">
                                        <li class="list-inline-item d-inline-flex align-items-center mb-2 me-4"><i
                                                class="bi-check-circle-fill me-2 color"></i>Smart attendance with
                                            QR Code technology</li>
                                        <li class="list-inline-item d-inline-flex align-items-center mb-2 me-4"><i
                                                class="bi-check-circle-fill me-2 color"></i>Easily and accurately
                                            monitor employee attendance</li>
                                        <li class="list-inline-item d-inline-flex align-items-center mb-2 me-4"><i
                                                class="bi-check-circle-fill me-2 color"></i>Improve efficiency and
                                            transparency in attendance tracking</li>
                                    </ul>
                                </div>
                                <img src="https://res.cloudinary.com/boxity-id/image/upload/w_1000/q_auto:best/f_auto/v1703267358/resources_new_boxity/kjlyhbjbcgojcrxxlzej.png"
                                    alt="Boxity Octans IDN" class="pe-6">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="grid-inner h-100 bg-white-soft rounded-5 d-flex flex-column"
                                style="background: linear-gradient(to bottom, rgba(0,0,0,0.65) 25%, transparent), url('https://res.cloudinary.com/boxity-id/image/upload/w_1000/q_auto:best/f_auto/v1703267355/resources_new_boxity/pipcisxxrblptvjtnz10.jpg') no-repeat center / cover">
                                <div class="p-5">
                                    <h3 class="mb-4 h3 fw-bold text-white">Boxity - Crater</h3>
                                    <p class="">Make Invoicing a Breeze for Your Business!</p>

                                    <ul class="list-inline">
                                        <li class="list-inline-item d-inline-flex align-items-center mb-2 me-4"><i
                                                class="bi-check-circle-fill me-2 color"></i>Quickly and efficiently
                                            create invoices</li>
                                        <li class="list-inline-item d-inline-flex align-items-center mb-2 me-4"><i
                                                class="bi-check-circle-fill me-2 color"></i>Perfect for individual
                                            businesses or small enterprises</li>
                                        <li class="list-inline-item d-inline-flex align-items-center mb-2 me-4"><i
                                                class="bi-check-circle-fill me-2 color"></i>Attractive design to
                                            make your business look pro!</li>
                                    </ul>
                                </div>
                                <img src="https://res.cloudinary.com/boxity-id/image/upload/w_1000/q_auto:best/f_auto/v1703267358/resources_new_boxity/fppjetvg6a5gs3ejjxps.png"
                                    alt="Boxity Crater" class="ps-5 mt-auto">
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div class="clear"></div>

            <div class="section"
                style="background: url('https://res.cloudinary.com/boxity-id/image/upload/w_1000/q_auto:best/f_auto/v1703267357/resources_new_boxity/yxj5rnjivfmxxoj2ylla.jpg') no-repeat center center / cover;">
                <div class="row text-center justify-content-center align-items-center min-vh-75 m-0">
                    <div class="col-md-8">
                        <h2 class="display-4 fw-semibold text-white">Empower your business with BoxityERP's
                            cutting-edge solutions, where innovation meets simplicity and efficiency</h2>
                        <div><a href="#" class="button text-black bg-color rounded-pill m-0 h-op-09 px-4">Try for 30
                                days
                                now</a></div>
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-8sony d-flex align-items-center flex-row m-5">
                        <div class="col-md-3 p-3 pt-2"><img src="{!! asset('webpage/demos/conference/images/sponcors/octans.png') !!}" alt="Clients Boxity">
                        </div>
                        <div class="col-md-3 p-3 pt-2"><img src="{!! asset('webpage/demos/conference/images/sponcors/flip.png') !!}" alt="Clients Boxity">
                        </div>
                        <div class="col-md-3 p-3 pt-2"><img src="{!! asset('webpage/demos/conference/images/sponcors/afindo.png') !!}" alt="Clients Boxity">
                        </div>
                        <div class="col-md-3 p-3 pt-2"><img src="{!! asset('webpage/demos/conference/images/sponcors/contabo.png') !!}" alt="Clients Boxity">
                        </div>
                    </div>
                </div>
            </div>

            <div class="section overflow-visible bg-transparent my-0 my-lg-5 pb-0 pb-lg-6">
                <div class="container">
                    <div class="row justify-content-center mb-4">

                        <div class="stack-cards col-lg-8 text-center">
                            <div class="sticky-title px-6">
                                <p class="color mb-3">Featured Clients</p>
                                <h2 class="display-5 fw-semibold">Reviews that will make you choose
                                    <span>BoxityID</span>
                                </h2>
                            </div>

                            <article class="stack-cards-item p-4 p-lg-5" aria-label="canvas Card - 1">
                                <div class="mb-4 color">
                                    <i class="bi-star-fill"></i>
                                    <i class="bi-star-fill"></i>
                                    <i class="bi-star-fill"></i>
                                    <i class="bi-star-fill"></i>
                                    <i class="bi-star-fill"></i>
                                </div>
                                <h2 class="fw-bold mb-4 lh-base">A Stylish Business Companion!</h2>
                                <p class="lead text-white-50">"BoxityERP isn't just business software; it's a true
                                    business companion! From inventory management to finance, everything becomes
                                    easier and more efficient. It's making my fashion business even more stylish!"
                                </p>

                                <div class="d-flex align-items-center mt-4">
                                    <img src="{!! asset('webpage/demos/finance/images/users/1.jpg') !!}" alt="User Boxity" width="60"
                                        class="rounded-circle">
                                    <div class="ms-3">
                                        <h4 class="font-body mb-2 fw-bold h6">Liana S.</h4>
                                        <div class="op-05 text-uppercase ls-2 text-smaller">Fashion Entrepreneur
                                        </div>
                                    </div>
                                </div>
                            </article>

                            <article class="stack-cards-item p-4 p-lg-5" aria-label="canvas Card - 2">
                                <div class="mb-4 color">
                                    <i class="bi-star-fill"></i>
                                    <i class="bi-star-fill"></i>
                                    <i class="bi-star-fill"></i>
                                    <i class="bi-star-fill"></i>
                                    <i class="bi-star-fill"></i>
                                </div>
                                <h3 class="fw-bold mb-4">Speed and Great Accuracy</h3>
                                <p class="lead text-white-50">"EnckeAtt by BoxityERP has made employee attendance
                                    management a breeze. Its QR scan is fast and accurate, giving us more time to
                                    focus on our exceptional coffee creations!""</p>

                                <div class="d-flex align-items-center mt-4">
                                    <img src="{!! asset('webpage/demos/finance/images/users/2.jpg') !!}" alt="User Boxity" width="60"
                                        class="rounded-circle">
                                    <div class="ms-3">
                                        <h4 class="font-body mb-2 fw-bold h6">Rizki P.</h4>
                                        <div class="op-05 text-uppercase ls-2 text-smaller">Hipster Café Owner
                                        </div>
                                    </div>
                                </div>
                            </article>
                            <article class="stack-cards-item p-4 p-lg-5" aria-label="canvas Card - 3">
                                <div class="mb-4 color">
                                    <i class="bi-star-fill"></i>
                                    <i class="bi-star-fill"></i>
                                    <i class="bi-star-fill"></i>
                                    <i class="bi-star-fill"></i>
                                    <i class="bi-star-fill"></i>
                                </div>
                                <h3 class="fw-bold mb-4">Creative Invoicing for a Cool Business!</h3>
                                <p class="lead text-white-50">"Crater from BoxityERP has helped our startup achieve
                                    professionalism. The invoices are cool and easily customizable, enhancing our
                                    business branding!"</p>

                                <div class="d-flex align-items-center mt-4">
                                    <img src="{!! asset('webpage/demos/finance/images/users/3.jpg') !!}" alt="User Boxity" width="60"
                                        class="rounded-circle">
                                    <div class="ms-3">
                                        <h4 class="font-body mb-2 fw-bold h6">Dian N.</h4>
                                        <div class="op-05 text-uppercase ls-2 text-smaller">Creative Startup
                                            Manager</div>
                                    </div>
                                </div>
                            </article>
                            <article class="stack-cards-item p-4 p-lg-5" aria-label="canvas Card - 4">
                                <div class="mb-4 color">
                                    <i class="bi-star-fill"></i>
                                    <i class="bi-star-fill"></i>
                                    <i class="bi-star-fill"></i>
                                    <i class="bi-star-fill"></i>
                                    <i class="bi-star-fill"></i>
                                </div>
                                <h3 class="fw-bold mb-4">Revitalizing Production with BoxityERP</h3>
                                <p class="lead text-white-50">"BoxityERP Production Management is transforming how
                                    we operate. Production efficiency is up, waste is down. It's not just software;
                                    it's a revolutionary innovation for the manufacturing industry"</p>

                                <div class="d-flex align-items-center mt-4">
                                    <img src="{!! asset('webpage/demos/finance/images/users/1.jpg') !!}" alt="User Boxity" width="60"
                                        class="rounded-circle">
                                    <div class="ms-3">
                                        <h4 class="font-body mb-2 fw-bold h6">Bambang W.</h4>
                                        <div class="op-05 text-uppercase ls-2 text-smaller">Manufacturing Director
                                        </div>
                                    </div>
                                </div>
                            </article>
                        </div>
                    </div>

                </div>

            </div>

            <div class="clear"></div>

            <div class="container mt-5">
                <div class="promo rounded-5 overflow-hidden"
                    style="background: linear-gradient(360deg, #040d1d, var(--cnvs-themecolor));">
                    <div class="row align-items-end justify-content-between">
                        <div class="col text-center">
                            <div class="p-5 p-lg-6">
                                <h2 class="text-light">Try all product of Boxity for 30 Days &<br> you'll never
                                    regret it!
                                </h2>
                                <a href="#"
                                    class="button button-large rounded-pill button-dark button-black border-0 m-0">Get
                                    started</a>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <img src="{!! asset('webpage/demos/integro/images/promo.png') !!}" alt="promo boxity" class="mt-5">
                        </div>
                        <div class="col-lg-1"></div>
                    </div>
                </div>
            </div>

        </div>

        <div class="clear mb-4"></div>
    </section><!-- #content end -->
@endsection
