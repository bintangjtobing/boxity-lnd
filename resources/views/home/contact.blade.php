@extends('welcome')
@section('title', 'Contact Us')
@section('page_description',
    'Boxity is a leading provider of software solutions for businesses of all sizes. We are committed to providing our
    customers with the best possible service.

    If you have any questions or need help with our products or services, please contact us. We are always happy to help.')

@section('icon',
    'https://res.cloudinary.com/boxity-id/image/upload/w_1000/q_auto:best/f_auto/v1678791753/asset_boxity/logo/icon-web_qusdsv.png')
@section('content')
    <section id="content">
        <div class="content-wrap">
            <div class="container">

                <div class="row col-mb-50 mb-0">
                    <div class="col-lg-4">

                        <div class="heading-block fancy-title border-bottom-0 title-bottom-border">
                            <h4>Contact Us</h4>
                        </div>
                        <address>
                            <strong>Headquarters:</strong><br>
                            Grand Slipi Tower, Jl. Jend Jl. Jelambar Barat No.22-24 Lt 9 Unit O,<br> Jelambar Baru, Kec.
                            Grogol
                            petamburan,<br>Kota Jakarta Barat, Daerah Khusus Ibukota Jakarta 11480
                        </address>
                        <abbr title="Phone Number"><strong>Phone:</strong></abbr> (62) 812 6284 5980<br>
                        <abbr title="Email Address"><strong>Email:</strong></abbr> info@boxity.id

                    </div>

                </div>

            </div>
        </div>
    </section>
@endsection
