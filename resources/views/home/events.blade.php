@extends('welcome')
@section('title', 'Events')
@section('page_description',
    'Boxity is a technology company that develops software solutions for businesses. We are always looking for ways to
    connect with our customers and partners, and our events are a great way to do that.

    Our events cover a variety of topics, including product updates, industry trends, and best practices. We also host
    networking events and social gatherings for our customers and community.

    If you want to stay up-to-date on the latest news from Boxity, be sure to check out our events page.')

@section('icon',
    'https://res.cloudinary.com/boxity-id/image/upload/w_1000/q_auto:best/f_auto/v1678791753/asset_boxity/logo/icon-web_qusdsv.png')
@section('content')
    <section class="page-title page-title-parallax parallax scroll-detect dark">
        <img src="https://res.cloudinary.com/boxity-id/image/upload/w_1000/q_auto:best/f_auto/v1703748942/parallax_tuucku.jpg"
            class="parallax-bg">
        <div class="container">
            <div class="page-title-row">

                <div class="page-title-content">
                    <h1>Events List</h1>
                </div>

                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Pages</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Events</li>
                    </ol>
                </nav>

            </div>
        </div>
    </section>
    <section id="content">
        <div class="content-wrap">
            <div class="container">
                <div class="row col-mb-50">
                    @forelse ($events as $item)
                    <main class="postcontent col-lg-9">

                        <div class="row g-4 mb-5">

                            <article class="entry event col-12 mb-0">
                                <div
                                    class="grid-inner bg-white row g-0 p-3 border-0 rounded-5 shadow-sm h-shadow all-ts h-translate-y-sm">
                                    <div class="col-md-4 mb-md-0">
                                        <a href="{{ $item->link_reg ? '#' : '#' }}" class="entry-image mb-0 h-100">
                                            <img src="{{ $item->image }}"
                                                alt="Boxity Central Indonesia Events - {{ $item->name }}"
                                                class="rounded-2 h-100 object-cover">
                                                <div class="bg-overlay">
                                                    <div class="bg-overlay-content justify-content-start align-items-start">
                                                        @if ($item->event_category)
                                                            <div class="badge bg-light text-dark rounded-pill">{{ $item->event_category['name'] }}</div>
                                                        @else
                                                            <div class="badge bg-light text-dark rounded-pill">Training Class</div>
                                                        @endif
                                                    </div>
                                                </div>

                                        </a>
                                    </div>
                                    <div class="col-md-8 p-4">
                                        <div class="entry-meta no-separator mb-1 mt-0">
                                            <ul>
                                                <li><a href="{{ $item->link_reg ? '#' : '#' }}" class="text-uppercase fw-medium"
                                                        style="color:black;">{{ \Carbon\Carbon::parse($item->start_date)->format('D, M j') }} @
                                                        {{ \Carbon\Carbon::parse($item->start_time)->format('g:iA') }}</a></li>
                                            </ul>
                                        </div>

                                        <div class="entry-title nott">
                                            <h3><a href="{{ $item->link_reg ? $item->link_reg : '#' }}"
                                                    style="color: black; text-decoration: none;
                                                :hover { color: orange; }">{{ $item->name }}</a></h3>

                                        </div>
                                        <div class="entry-content my-3">
                                            {!! $item->description !!}
                                        </div>

                                        <div class="entry-meta no-separator">
                                            <ul>
                                                <li><a href="#" class="fw-normal" style="color: black;"><i
                                                            class="uil uil-map-marker"></i>
                                                        {{$item->location}}</a></li>
                                                        <li><a href="{{ $item->link_reg ? $item->link_reg : '#' }}" target="_blank" class="fw-normal button text-black bg-color rounded-pill m-0 h-op-09 px-4">
                                                            Registrasi Disini</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </article>

                        </div>

                    </main>
                    @empty
                    <div class="fancy-title title-bottom-border">
                        <p>We appreciate your interest in our events.<br>We currently have no available events here, but we
                            will be sure to let you know when we do.
                        </p>
                    </div>
                    @endforelse
                </div>

            </div>
        </div>
    </section><!-- #content end -->
@endsection
