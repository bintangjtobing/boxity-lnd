<!DOCTYPE html>
<html dir="ltr" lang="en-US">

<head>

    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <meta http-equiv="x-ua-compatible" content="IE=edge">
    <meta name="author" content="@yield('title') | PT Boxity Central Indonesia" />
    <meta name="description" content="@yield('page_description')" />
    <meta name="keywords"
        content="Boxity ERP,ERP for businesses,ERP in Indonesia,Cloud ERP,SaaS ERP,Best ERP for businesses,Most affordable ERP SaaS,ERP for small businesses,ERP for medium businesses,ERP for large businesses,ERP for manufacturing businesses,ERP for retail businesses,ERP for service businesses,aplikasi keuangan,software keuangan,akuntansi software,software keuangan untuk bisnis,aplikasi keuangan untuk bisnis,aplikasi keuangan cloud,aplikasi keuangan SaaS,aplikasi keuangan terbaik,aplikasi keuangan terjangkau,aplikasi keuangan untuk UMKM,aplikasi keuangan untuk UKM,aplikasi keuangan untuk perusahaan,aplikasi attendance,aplikasi absensi,aplikasi kehadiran,software attendance,software absensi,software kehadiran,aplikasi attendance cloud,aplikasi attendance SaaS,aplikasi attendance terbaik,aplikasi attendance terjangkau,aplikasi attendance untuk karyawan,aplikasi attendance untuk pegawai,aplikasi invoicing,software invoicing,aplikasi faktur,software faktur,aplikasi billing,software billing,aplikasi invoicing cloud,aplikasi invoicing SaaS,aplikasi invoicing terbaik,aplikasi invoicing terjangkau,aplikasi invoicing untuk bisnis,aplikasi invoicing untuk UMKM,aplikasi invoicing untuk UKM" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="copyright" content="PT Boxity Central Indonesia">
    <meta name="url" content="{{ Request::url() }}">
    <meta name="identifier-URL" content="{{ Request::url() }}">

    <!-- Font Imports -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Manrope:wght@400;500&family=PT+Serif:wght@400;700&display=swap"
        rel="stylesheet">

    <!-- Core Style -->
    <link rel="stylesheet" href="{!! asset('webpage/style.css') !!}">

    <!-- Font Icons -->
    <link rel="stylesheet" href="{!! asset('webpage/css/font-icons.css') !!}">

    <!-- Niche Demos -->
    <link rel="stylesheet" href="{!! asset('webpage/demos/integro/integro.css') !!}">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="{!! asset('webpage/css/custom.css') !!}">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Document Title
 ============================================= -->
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-VLZMGJP9FS"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'G-VLZMGJP9FS');
    </script>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-217791541-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'UA-217791541-1');
    </script>

    <!-- SITE TITLE -->
    <title>@yield('title') - BoxityERP by Boxity Central Indonesia</title>
    <!-- FAVICON AND TOUCH ICONS -->
    <link rel="icon"
        href="@if (View::hasSection('icon')) @yield('icon') @else https://res.cloudinary.com/boxity-id/image/upload/w_1000/q_auto:best/f_auto/v1678791753/asset_boxity/logo/icon-web_qusdsv.png @endif"
        type="image/png">
    <link rel="apple-touch-icon" sizes="152x152"
        href="https://res.cloudinary.com/boxity-id/image/upload/w_1000/q_auto:best/f_auto/v1678791753/asset_boxity/logo/icon-web_qusdsv.png">
    <link rel="apple-touch-icon" sizes="120x120"
        href="https://res.cloudinary.com/boxity-id/image/upload/w_1000/q_auto:best/f_auto/v1678791753/asset_boxity/logo/icon-web_qusdsv.png">
    <link rel="apple-touch-icon" sizes="76x76"
        href="https://res.cloudinary.com/boxity-id/image/upload/w_1000/q_auto:best/f_auto/v1678791753/asset_boxity/logo/icon-web_qusdsv.png">

    {{-- Meta --}}
    <meta name="og:title" property="og:title" content="@yield('title') | PT Boxity Central Indonesia" />
    <meta name="og:url" property="og:url" content="{{ Request::url() }}" />
    <meta name="og:type" property="og:type" content="website" />
    <meta name="og:image" property="og:image"
        content="@if (View::hasSection('tag_cover')) @yield('tag_cover')@else https://res.cloudinary.com/boxity-id/image/upload/w_1000/q_auto:best/f_auto/v1642815378/assets/site%20needs/tag_cover_hsiapv.jpg @endif" />
    <meta name="og:description" property="og:description" content="@yield('page_description')" />
    <meta name="twitter:card" content="@yield('title') | PT Boxity Central Indonesia" />
    <meta name="twitter:title" property="og:title" content="@yield('title') | PT Boxity Central Indonesia" />
    <meta name="twitter:url" property="og:url" content="{{ Request::url() }}" />
    <meta name="twitter:type" property="og:type" content="website" />
    <meta name="twitter:image" property="og:image"
        content="@if (View::hasSection('tag_cover')) @yield('tag_cover')@else https://res.cloudinary.com/boxity-id/image/upload/w_1000/q_auto:best/f_auto/v1642815378/assets/site%20needs/tag_cover_hsiapv.jpg @endif" />
    <meta name="twitter:description" property="og:description" content="@yield('page_description')" />
    <link rel="canonical" href="{{ Request::url() }}/" />
    <link rel="shortcut icon"
        href="@if (View::hasSection('icon')) @yield('icon')@else https://res.cloudinary.com/boxity-id/image/upload/w_1000/q_auto:best/f_auto/v1678791753/asset_boxity/logo/icon-web_qusdsv.png @endif"
        type="image/png" sizes="64x64" />
    <meta name="og:email" content="info@boxity.id" />
    <meta name="og:phone_number" content="02129021873" />
    <meta name="og:latitude" content="-6.1465558" />
    <meta name="og:longitude" content="106.7843094" />
    <meta name="og:street-address" content="Grand Silipi Tower, Jl. Jend Jl. Jelambar Barat No.22-24" />
    <meta name="og:locality" content="DKI Jakarta" />
    <meta name="og:region" content="ID" />
    <meta name="og:postal-code" content="11480" />
    <meta name="og:country-name" content="Indonesia" />

</head>

<body class="stretched dark">

    <!-- Document Wrapper
 ============================================= -->
    <div id="wrapper">

        <!-- Header
  ============================================= -->
        <header id="header" class="dark border-0">
            <div id="header-wrap">
                <div class="container">
                    <div class="header-row">

                        <!-- Logo
      ============================================= -->
                        <div id="logo">
                            <a href="/"><img
                                    src="https://res.cloudinary.com/boxity-id/image/upload/w_1000/q_auto:best/f_auto/v1703264204/resources_new_boxity/znhxlwmqju2txa09bsmm.png"
                                    alt="Boxity Central Indonesia Logo" class="py-3"></a>
                        </div><!-- #logo end -->

                        <div class="primary-menu-trigger">
                            <button class="cnvs-hamburger" type="button" title="Open Mobile Menu">
                                <span class="cnvs-hamburger-box"><span class="cnvs-hamburger-inner"></span></span>
                            </button>
                        </div>

                        <!-- Primary Navigation
      ============================================= -->
                        <nav class="primary-menu with-arrows">

                            <ul class="menu-container">
                                <li class="menu-item {{ request()->is('/') ? 'current' : '' }}"><a class="menu-link"
                                        href="/">
                                        <div>Home</div>
                                    </a></li>
                                <li class="menu-item {{ request()->is('products/') ? 'current' : '' }}"><a
                                        class="menu-link" href="#">
                                        <div>Product</div>
                                    </a>
                                    <ul class="sub-menu-container rounded-5" data-class="up-lg:not-dark">
                                        <li class="menu-item"><a class="menu-link" href="/products/boxityerp">
                                                <div>BoxityERP</div>
                                            </a></li>
                                        <li class="menu-item"><a class="menu-link" href="/products/octansidn">
                                                <div>Octans</div>
                                            </a></li>
                                        <li class="menu-item"><a class="menu-link" href="/products/enckeatt">
                                                <div>EnckeAtt</div>
                                            </a></li>
                                        <li class="menu-item"><a class="menu-link" href="/products/crater">
                                                <div>Crater</div>
                                            </a></li>
                                    </ul>
                                </li>
                                <li class="menu-item {{ request()->is('career') ? 'current' : '' }}"><a
                                        class="menu-link" href="/career">
                                        <div>Career</div>
                                    </a></li>
                                <li class="menu-item {{ request()->is('about') ? 'current' : '' }}"><a
                                        class="menu-link" href="/about">
                                        <div>About</div>
                                    </a></li>
                            </ul>

                        </nav><!-- #primary-menu end -->

                    </div>
                </div>
            </div>
            <div class="header-wrap-clone"></div>
        </header><!-- #header end -->

        <!-- Content
  ============================================= -->
        @yield('content')

        <!-- Footer
  ============================================= -->
        <footer id="footer" class="border-width-1">
            <div class="container">

                <!-- Footer Widgets
    ============================================= -->
                <div class="footer-widgets-wrap">
                    <div class="row">

                        <div class="col-lg-2 col-md-2 col-6 mb-5 mb-md-0">
                            <div class="widget widget_links widget-li-noicon">

                                <h4 class="ls-0 mb-4 text-transform-none">Products</h4>

                                <ul class="list-unstyled iconlist ms-0">
                                    <li><a href="/products/boxityerp">BoxityERP</a></li>
                                    <li><a href="/products/octansidn">Octans Finance</a></li>
                                    <li><a href="/products/enckeatt">Encke Attendances</a></li>
                                    <li><a href="/products/crater">Crater Invoicing</a></li>
                                </ul>

                            </div>
                        </div>
                        <div class="col-lg-2 col-md-2 col-6 mb-5 mb-md-0">
                            <div class="widget widget_links widget-li-noicon">

                                <h4 class="ls-0 mb-4 text-transform-none">FAQ</h4>

                                <ul class="list-unstyled iconlist ms-0">
                                    <li><a href="/about">About</a></li>
                                    <li><a href="/contact">Support</a></li>
                                </ul>

                            </div>
                        </div>
                        <div class="col-lg-2 col-md-2 col-6 mb-5 mb-md-0">
                            <div class="widget widget_links widget-li-noicon">

                                <h4 class="ls-0 mb-4 text-transform-none">Company</h4>

                                <ul class="list-unstyled iconlist ms-0">
                                    <li><a href="/career">Career</a></li>
                                    <li><a href="/events">Events</a></li>
                                    <li><a href="/profile">Company Profile</a></li>
                                    {{-- <li><a href="#">Forums</a></li> --}}
                                    {{-- <li><a href="/partnership">Partnership</a></li> --}}
                                </ul>

                            </div>
                        </div>
                        <div class="col-lg-2 col-md-2 col-6 mb-5 mb-md-0">
                            <div class="widget widget_links widget-li-noicon">

                                <h4 class="ls-0 mb-4 text-transform-none">Contact Us</h4>

                                <ul class="list-unstyled iconlist ms-0">
                                    <li><a href="https://wa.me/6281262845980">+62 812 6284 5980 (Whatsapp)</a></li>
                                    <li><a href="mailto:info@boxity.id">info@boxity.id</a></li>
                                    <li><a href="#">24x7 Online support</a></li>
                                </ul>

                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 text-md-end">
                            <div class="widget">
                                <div>
                                    <address>
                                        Grand Slipi Tower, Lt 9 Unit O<br>
                                        Kota Jakarta Barat, DKI Jakarta 11480<br>
                                    </address>
                                    <div class="d-flex justify-content-md-end">
                                        <a href="https://facebook.com/boxitycentralindonesia"
                                            class="social-icon si-small bg-facebook" title="Facebook">
                                            <i class="fa-brands fa-facebook-f"></i>
                                            <i class="fa-brands fa-facebook-f"></i>
                                        </a>

                                        <a href="https://instagram.com/boxityid"
                                            class="social-icon si-small bg-instagram" title="instagram">
                                            <i class="bi-instagram"></i>
                                            <i class="bi-instagram"></i>
                                        </a>

                                        <a href="https://linkedin.com/company/boxity-central-indonesia"
                                            class="social-icon si-small bg-linkedin" title="linkedin">
                                            <i class="fa-brands fa-linkedin"></i>
                                            <i class="fa-brands fa-linkedin"></i>
                                        </a>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>

                </div><!-- .footer-widgets-wrap end -->

            </div>

            <!-- Copyrights
   ============================================= -->
            <div id="copyrights">
                <div class="container">

                    <div class="row justify-content-between align-items-center">
                        <div class="col-md-6">
                            <?php $y = date('Y'); ?>
                            Copyrights &copy;{{ $y }} Boxity | PT. Boxity Central Indonesia.
                        </div>

                        <div class="col-md-6 d-md-flex flex-md-column align-items-md-end mt-4 mt-md-0">
                            <a href="/contact" class="button rounded-pill bg-danger border-0 m-0">Help?</a>
                        </div>
                    </div>

                </div>
            </div><!-- #copyrights end -->
        </footer><!-- #footer end -->

    </div><!-- #wrapper end -->

    <!-- Go To Top
 ============================================= -->
    <div id="gotoTop" class="uil uil-angle-up"></div>

    <!-- JavaScripts
 ============================================= -->
    <script src="{!! asset('webpage/js/plugins.min.js') !!}"></script>
    <script src="{!! asset('webpage/js/functions.bundle.js') !!}"></script>
    <!-- Charts JS
 ============================================= -->
    <script src="{!! asset('webpage/js/chart.js') !!}"></script>
    <script src="{!! asset('webpage/js/chart-utils.js') !!}"></script>

    <script>
        jQuery(window).on('load', function() {
            jQuery('#oc-testi').owlCarousel({
                items: 1,
                margin: 25,
                nav: true,
                navText: ['<span class="h-text-contrast-200">&#10229</span>',
                    '<span class="h-text-contrast-200">&#10230</span>'
                ],
                dots: false,
                responsive: {
                    576: {
                        items: 1
                    },
                    768: {
                        items: 2
                    }
                },
            });
        });
    </script>
    @yield('script')

</body>

</html>
