<?php

namespace App\Http\Controllers;

use App\event;
use App\eventCategories;
use Illuminate\Http\Request;

class eventController extends Controller
{
    public function index(){
        $events = event::with('eventCategory')->orderBy('start_date', 'desc')->paginate(10);
    return view('home.events', compact('events'));
// return response()->json($events);
    }
}