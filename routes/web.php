<?php

use Illuminate\Support\Str;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Redirect;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Route utama
Route::get('/','webpageController@index')->name('Home');
Route::get('/verified/internship/unpri/2023', function () {
    return response()->json([
        'status' => 200,
        'data' => Str::uuid(),
        'message' => 'Participation of internship at Boxity 2023 was confirmed.',
    ], 200);
});
Route::get('/about', function () {
    return view('home.about');
})->name('About');
Route::get('/contact', function () {
    return view('home.contact');
})->name('Contact');
Route::get('/career', function () {
    return view('home.career');
})->name('Career');
Route::get('/events', 'eventController@index')->name('Event');
Route::get('/profile', function () {
    return Redirect::to('/businesses/boxityERP_profile.pdf');
});
Route::prefix('sc/business')->group(function () {
    Route::get('/pitch/2022/en', function () {
        return Redirect::to('/pitch/Eng_pitch_deck_2022.pdf');
    });
    Route::get('/pitch/2022/id', function () {
        return Redirect::to('/pitch/Ind_pitch_deck_2022.pdf');
    });
    Route::get('/pitch/2023/id', function () {
        return Redirect::to('/pitch/Ind_pitch_deck_2023.pdf');
    });
    Route::get('/pitch/2022', function () {
        return Redirect::to('/pitch/Ind_pitch_deck_q42022.pdf');
    });
});

// Route produk
Route::prefix('products')->group(function () {
    Route::get('boxityerp', function () {
        return view('home.products.erp');
    })->name('products.boxityerp');

    Route::get('octansidn', function () {
        return view('home.products.octansidn');
    })->name('products.octansidn');

    Route::get('enckeatt', function () {
        return view('home.products.enckeatt');
    })->name('products.enckeatt');
    Route::get('crater', function () {
        return view('home.products.crater');
    })->name('products.crater');
});